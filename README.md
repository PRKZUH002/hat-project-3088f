# Multi Sensing Circuit

Multi Sensing Circuit with an Analogue Sensor (LDR) and a Digital Sensor (Hal).
It can detect light and a magnetic connection. This can be used for an alarm system or to detect when it is night time (For Garden Lights).

## Built With
•	KiCad
•	STM32IDE
•	GitHub
•	JLCPCB

## Getting started
Prerequisites

Hardware needed: STM32F051R8T6

Software needed: KiCad, STM32IDE and Git

## Installation
STM32F051R8T6 needed to communicate with PCB

STM32IDE need to control microcontroller

## Usage
(Analogue Sensor) Light Sensor: Detect when it is night-time
(Hal Sensor) Alarm System: Detect when something is open

For more examples, please refer to the Documentation

## Support
Support can be found in the discord

## Roadmap
1. Remove errors
2. Simplify Design

## Contributing
We welcome any contributions. The requirements for contributions can be found here.

To contribute:
1.	Fork the project
2.	Create branch
3.	Commit changes
4.	Push to Branch
5.	Open a Pull Request

## Authors and acknowledgment
Authors: Zuhayr Parkar, M. Zaakir Ebrahim and Yousuf Isaacs

EEE3088F Teaching Staff

## License
Distributed under the [UCT_license] license

## Project status
Completed
