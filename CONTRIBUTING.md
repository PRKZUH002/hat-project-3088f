## Contribute to Light and Magnet Detection
Thank you for your interest in contributing. Below, is an easy guide on how to contribute.

## Code of Conduct 
This is a welcoming environment for all that wants to contribute to this project. Refer to Code of Conduct.

## Merge Requests
Make sure all guidelines are followed. Any requests that do not meet the guidelines will not be considered.

## Bugs
It is much appreciated it bugs are reported.

## How to Contribute
1.	Create a fork
2.	Make Your Changes
3.	Create a merge request
4.	Merge Request: Have accurate information
5.	Assign the request to one of the owners and explain that you are ready for review.

Send merge requests to all three owners.

## Styles guides
This link outlines the current design style used.

## Implement Design and UI elements
This link outlines the design and UI currently used.
Contribute Documentation
For information on how to contribute documentation, see link.

## Getting a License
Information on how to obtain a license.
