Code reference
STM32f0xx_it.c
void NMI_Handler(void)
This function has no parameters and is blank

void SVC_Handler(void)
This function has no parameters and is blank

void PendSV_Handler(void)
This function has no parameters and is blank

void SysTick_Handler(void)
This function contains code to handle the Systems tick timer. This function has no parameters.
Main.c

void Error_Handler(void)
This function contains code to report the HAL error return state. This function has no parameters.

static void MX_GPIO_Init(void)
This function contains code to initialize all the GPIO pins used on the uC and establish their uses. This function has no parameters.

static void MX_USART2_Init(void)
this function contains code to initialize the USART pins. This function has no parameters.




static void MX_I2C2_Init(void)
This function contains code to initialize uC pins for I2C use. This function has no parameters.

static void MX_ADC_Init(void)
This function contains code to Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion). This function has no parameters.

void SystemClock_Config(void)
This function contains code to Initializes the RCC Oscillators according to the specified parameters in the RCC_OscInitTypeDef structure. This function has no parameters.

int main(void)
This function contains the bulk of the code. It contains code to tell the uC what to do with the inputs of the GPIO pins as well as what to do with the inputs of the ADC. Additionally, it tells the uC which data to send via I2C to the eeprom and receives data from the eeprom to send to the USB via the FTDI

HAL_I2C_Mem_Read (I2C_HandleTypeDef * hi2c, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t * pData, uint16_t Size, uint32_t Timeout)
This function is in charge of getting all the memory addresses and then using then to write data to the eeprom via the I2C protocol
